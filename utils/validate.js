export const validateInputRequired = (value, field, res) => {
  if (!value) {
    return res.status(422).json({
      errors: {
        [field]: 'is required'
      }
    });
  }
};

export default {};
