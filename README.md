# NodeJS + Express + Passport

1. Install NodeJS
2. Install MongoDB
3. Run mongo
`"path_to_mongo\mongo.exe" nodejs-auth`<br>
Example: `"C:\Program Files\MongoDB\Server\4.0\bin\mongo.exe" nodejs-auth`
4. Install dependencies. Run `npm install`
5. Install nodemon. Run `npm install -g nodemon`
6. Start server. Run `npm start`
7. Test: in [Postman](https://www.getpostman.com) or in [Google Map Project](https://gitlab.com/abylitskaya/google-maps-angular)

Links:
- https://nodejs.org
- https://nodemon.io
- https://www.mongodb.com
- https://expressjs.com
- http://www.passportjs.org
- https://www.getpostman.com
- https://www.npmjs.com/package/eslint
- https://www.npmjs.com/package/husky
- https://www.npmjs.com/package/lint-staged
- https://prettier.io
- https://prettier.io/docs/en/options.html
