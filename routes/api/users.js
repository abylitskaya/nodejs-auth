import mongoose from 'mongoose';
import passport from 'passport';
import express from 'express';

import auth from '../auth';
import { validateInputRequired } from '../../utils/validate';

const Users = mongoose.model('Users');
const router = express.Router();

// POST new user route (optional, everyone has access)
// Regestration
router.post('/', auth.optional, (req, res, next) => {
  const {
    body: { user }
  } = req;
  const errors =
    validateInputRequired(user, 'email', res) ||
    validateInputRequired(user.email, 'email', res) ||
    validateInputRequired(user.password, 'password', res);

  if (errors) {
    return errors;
  }

  const finalUser = new Users(user);

  finalUser.setPassword(user.password);

  return finalUser
    .save()
    .then(() => res.json(finalUser.toAuthJSON()));
});

// POST login route (optional, everyone has access)
// Authetication
router.post('/login', auth.optional, (req, res, next) => {
  const {
    body: { user }
  } = req;
  const errors =
    validateInputRequired(user, 'email', res) ||
    validateInputRequired(user.email, 'email', res) ||
    validateInputRequired(user.password, 'password', res);

  if (errors) {
    return errors;
  }

  return passport.authenticate(
    'local',
    { session: false },
    (err, passportUser, info) => {
      if (err) {
        return next(err);
      }

      if (passportUser) {
        const user = passportUser;
        user.token = passportUser.generateJWT();

        return res.json(user.toAuthJSON());
      }

    return res.json(info);
  })(req, res, next);
});

// GET current route (required, only authenticated users have access)
// Get current user
router.get('/current', auth.required, (req, res, next) => {
  const {
    query: { id }
  } = req;
  const errors = validateInputRequired(id, 'id', res);

  if (errors) {
    return errors;
  }

  return Users.findById(id)
    .then((user) => {
      if (!user) {
        return res.json({ user: null });
      }

      return res.json(user.toAuthJSON());
    });
});

export default router;
